# Introduction

A web application that can turn annotations from annotations tool other than BIIGLE into iFDO format.

### TODO

- Use Docker container, flask, python
- Create module for OFOP
- Create module for Squidle(?)
- Create module for Paparazzi(?)
- Enable uploading an annotation file
- Provide a form to add required iFDO metadata
- Create an iFDO file
- Enable downloading the created iFDO
